import { RECT_AREA, SQUARE_AREA } from './constants';

function reducer(state = {
    area: undefined
}, action) {
    switch (action.type) {
        case SQUARE_AREA:
            return {
                area: action.length * action.length
            }
        case RECT_AREA:
            return {
                area: action.length * action.breadth
            }
        default:
            return state;
    }
}

export default reducer;