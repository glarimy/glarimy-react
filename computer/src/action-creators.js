import {SQUARE_AREA, RECT_AREA} from './constants'

export function computeSquareArea(length) {
    return {
        type: SQUARE_AREA,
        length: length
    }
}

export function computeReactArea(length, breadth) {
    return {
        type: RECT_AREA,
        length: length,
        breadth: breadth
    }
}