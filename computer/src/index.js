import React from 'react';
import ReactDOM from 'react-dom';
import Computer from './components/Computer/';
import store from './store';
import {Provider} from 'react-redux';

ReactDOM.render(
    <Provider store={store}>
        <Computer />
    </Provider>, 
    document.getElementById('computer')
);