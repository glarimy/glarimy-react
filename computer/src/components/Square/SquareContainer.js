import React from 'react';
import SquareComponent from './SquareComponent';

class SquareContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            length: ""
        }
        this.onLengthChange = event => this.setState({ length: event.target.value });
        this.onCompute = event => {
            this.props.compute(this.state.length);
            this.props.history.push('/area');
        }
    }
    render() {
        return (
            <SquareComponent
                length={this.state.length}
                onLengthChange={this.onLengthChange}
                onCompute={this.onCompute}
            />
        );
    }
}

export default SquareContainer;