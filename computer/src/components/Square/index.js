import SquareContainer from './SquareContainer';
import { computeSquareArea } from '../../action-creators';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        compute: (l) => dispatch(computeSquareArea(l))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SquareContainer);