import React from 'react';

const AreaComponent = props => 
    <div>AREA: {props.area}</div>

export default AreaComponent;