import AreaComponent from './AreaComponent';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        area: state.area
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AreaComponent);