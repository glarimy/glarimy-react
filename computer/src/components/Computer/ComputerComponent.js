import React from 'react';
import Square from '../Square/';
import Reactangle from '../Reactangle';
import Area from '../Area';
import { Link, Switch, Route, HashRouter as Router } from 'react-router-dom';

const ComputerComponent = props =>
    <Router>
        <div>
            <h1>Computer</h1>
            <Link to='/square'>Square</Link> | <Link to='/rect'>Reactangle</Link>
            <hr />
            <Switch>
                <Route path='/square' component={Square} />
                <Route path='/rect' component={Reactangle} />
                <Route path='/area' component={Area} />
            </Switch>
        </div>
    </Router>
export default ComputerComponent;