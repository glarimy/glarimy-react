import RectangleContainer from './ReactangleContainer';
import { computeReactArea } from '../../action-creators';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        compute: (l, b) => dispatch(computeReactArea(l, b))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(RectangleContainer);