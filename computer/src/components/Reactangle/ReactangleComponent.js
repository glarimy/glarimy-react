import React from 'react';

const RectangleComponent = props =>
    <div>
        <h3>Rectangle</h3>
        <table>
            <tbody>
                <tr>
                    <td>Length</td>
                    <td><input value={props.length} onChange={props.onLengthChange} /></td>
                </tr>
                <tr>
                    <td>Breadth</td>
                    <td><input value={props.breadth} onChange={props.onBreadthChange} /></td>
                </tr>
                <tr>
                    <td colSpan='2' align='right'><button onClick={props.onCompute}>Compute</button></td>
                </tr>
            </tbody>
        </table></div>

export default RectangleComponent;