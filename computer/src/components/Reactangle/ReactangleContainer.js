import React from 'react';
import RectangleComponent from './ReactangleComponent';

class ReactangleContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            length: "",
            breadth: ""
        }
        this.onLengthChange = event => this.setState({ length: event.target.value });
        this.onBreadthChange = event => this.setState({ breadth: event.target.value });
        this.onCompute = event => {
            this.props.compute(this.state.length, this.state.breadth);
            this.props.history.push('/area');
        }
    }
    render() {
        return (
            <RectangleComponent
                length={this.state.length}
                breadth={this.state.breadth}
                onLengthChange={this.onLengthChange}
                onBreadthChange={this.onBreadthChange}
                onCompute={this.onCompute}
            />
        );
    }
}

export default ReactangleContainer;