import { createStore, combineReducers, applyMiddleware } from 'redux';
import adder from './adder';
import multiplier from './multiplier';
import logger from './logger';
import thunk from 'redux-thunk';

const store = createStore(
    combineReducers({adder, multiplier}), 
    applyMiddleware(logger, thunk)
);

export default store;