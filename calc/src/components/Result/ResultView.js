import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    border: 1px dotted gray;
    padding: 20px;
    background-color: red;
`
const Display = styled.div`
    color: white;
`
const ResultView = props => 
    <Wrapper>
        <Display>SUM: {props.sum}</Display>
        <Display>PRODUCT: {props.product}</Display>
    </Wrapper>
    
export default ResultView;