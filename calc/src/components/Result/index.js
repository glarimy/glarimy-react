import ResultView from './ResultView';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        sum: state.adder.sum,
        product: state.multiplier.product
    };
};

const mapDispatchToProps = (dispatch) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ResultView);