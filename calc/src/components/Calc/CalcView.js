import React from 'react';
import Form from '../Form';
import Result from '../Result';

const CalcView = props => 
    <div>
        <h2>Calculator</h2>
        <Form/>
        <hr/>
        <Result/>
    </div>

export default CalcView;