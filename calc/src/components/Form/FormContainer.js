import React from 'react';
import FormView from './FormView';

class FormContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstnumber: "",
            secondnumber: ""
        }
        this.onFirstNumberChagne = event => this.setState({ firstnumber: event.target.value });
        this.onSecondNumberChange = event => this.setState({ secondnumber: event.target.value });
        this.onCompute = event => this.props.compute(this.state.firstnumber, this.state.secondnumber);
    }
    render() {
        return (
            <FormView
                firstnumber={this.state.firstnumber}
                secondnumber={this.state.secondnumber}
                onFirstNumberChagne={this.onFirstNumberChagne}
                onSecondNumberChange={this.onSecondNumberChange}
                onCompute={this.onCompute}
            />
        );
    }
}

export default FormContainer;