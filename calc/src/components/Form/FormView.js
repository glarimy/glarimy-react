import React from 'react';
import styled from 'styled-components';

const FormTable = styled.table`
    & tr td {
        color: red;
    }
`

const FormView = props =>
    <FormTable>
        <tbody>
            <tr>
                <td>First Number</td>
                <td><input value={props.firstnumber} onChange={props.onFirstNumberChagne} /></td>
            </tr>
            <tr>
                <td>Second Number</td>
                <td><input value={props.secondnumber} onChange={props.onSecondNumberChange} /></td>
            </tr>
            <tr>
                <td colSpan='2' align='right'><button onClick={props.onCompute}>Compute</button></td>
            </tr>
        </tbody>
    </FormTable>

export default FormView;