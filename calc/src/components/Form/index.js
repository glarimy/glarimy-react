import FormContainer from './FormContainer';
import { compute } from '../../event-creators';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        compute: (fn, sn) => dispatch(compute(fn, sn))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);