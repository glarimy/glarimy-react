import React from 'react';
import ReactDOM from 'react-dom';
import Calc from './components/Calc/';
import store from './store';
import {Provider} from 'react-redux';

ReactDOM.render(
    <Provider store={store}>
        <Calc/>
    </Provider>, 
    document.getElementById("root")
);

