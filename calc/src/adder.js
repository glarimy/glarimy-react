import * as EVENTS from './constants';

function adder(state = {
    result: undefined
}, action) {
    switch (action.type) {
        case EVENTS.COMPUTE:
            return {
                sum: Number.parseInt(action.firstnumber) + Number.parseInt(action.secondnumber)
            }
        default: 
            return state;
    }
}

export default adder;