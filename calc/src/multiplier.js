import * as EVENTS from './constants';

function multiplier(state = {
    result: undefined
}, action) {
    switch (action.type) {
        case EVENTS.COMPUTE:
            return {
                product: action.firstnumber * action.secondnumber
            }
        default: 
            return state;
    }
}

export default multiplier;