import * as EVENTS from './constants'

export function compute(firstnumber, secondnumber) {
    /*
    return function(dispatch){
        fetch("/event", {
            method: "post",
            data: JSON.stringify({
                type: EVENTS.COMPUTE,
                firstnumber: firstnumber,
                secondnumber: secondnumber
            })
        }).then(response => {
            dispatch({
                type: EVENTS.COMPUTE,
                firstnumber: firstnumber,
                secondnumber: secondnumber
            });
        });
    }
    */
    return function (dispatch) {
        setTimeout(function () {
            dispatch({
                type: EVENTS.COMPUTE,
                firstnumber: firstnumber,
                secondnumber: secondnumber
            })
        }, 1000);
    }
}