import * as TYPES from './events.js';

export function addEmployees(employees) {
    return {
        type: TYPES.ADD_EMPLOYEES,
        employees: employees
    }
}

export function addEmployee(employee) {
    return dispatch => {
        fetch("employee", {
            "method": "post",
            body: JSON.stringify(employee)
        }).then(response => {
            dispatch({
                type: TYPES.ADD_EMPLOYEE,
                employee: employee
            });
        });
    }
}
/*
export function addEmployee(employee) {
    return {
        type: TYPES.ADD_EMPLOYEE,
        employee: employee
    }
}
*/
export function viewEmployee(phone) {
    return {
        type: TYPES.VIEW_EMPLOYEE,
        phone: phone
    }
}

export function removeEmployee(phone) {
    return {
        type: TYPES.REMOVE_EMPLOYEE,
        phone: phone
    }
}