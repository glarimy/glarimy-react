import React from 'react';
import ReactDOM from 'react-dom';
import Directory from './components/Directory/'
import store from './store';
import {Provider} from 'react-redux';

ReactDOM.render(
    <Provider store={store}>
        <Directory/>
    </Provider>, 
    document.getElementById("root")
);