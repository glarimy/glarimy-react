import * as TYPES from './events';

function reducer(state = {
    employees: [],
    employee: {
        name: "",
        phone: "",
        email: ""
    }
}, action) {
    switch (action.type) {
        case TYPES.ADD_EMPLOYEES: 
            return {
                employees: action.employees,
                employee: state.employee
            }
        case TYPES.ADD_EMPLOYEE:
            return {
                employees: [...state.employees, action.employee],
                employee: state.employee
            }
        case TYPES.VIEW_EMPLOYEE:
            const emp = state.employees.find(e => e.phone === action.phone);
            return {
                employees: state.employees,
                employee: {
                    name: emp.name,
                    phone: emp.phone,
                    email: emp.email
                }
            }
        case TYPES.REMOVE_EMPLOYEE:
            return {
                employees: state.employees.filter(e => e.phone !== action.phone),
                employee: action.phone === state.employee.phone ? {
                    name: "",
                    phone: "",
                    email: ""
                } : state.employee
            }

        default:
            return state;
    }
}

export default reducer;