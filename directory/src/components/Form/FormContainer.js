import React from 'react';
import FormView from './FormView';

class FormConatiner extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            phone: "",
            email: ""
        }

        this.onNameChange = e => this.setState({ name: e.target.value });
        this.onPhoneChange = e => this.setState({ phone: e.target.value });
        this.onEmailChange = e => this.setState({ email: e.target.value });
        this.onRegister = e => {
            this.props.addEmployee({
                name: this.state.name,
                phone: this.state.phone,
                email: this.state.email
            });
            this.setState({
                name: "",
                phone: "",
                email: ""
            });
            this.props.history.push('/list');
        }
    }

    render() {
        return (
            <FormView
                name={this.state.name}
                phone={this.state.phone}
                email={this.state.email}
                onNameChange={this.onNameChange}
                onPhoneChange={this.onPhoneChange}
                onEmailChange={this.onEmailChange}
                onRegister={this.onRegister}
            />
        );
    }
}

export default FormConatiner;