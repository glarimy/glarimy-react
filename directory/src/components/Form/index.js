import FormContainer from './FormContainer';
import {connect} from 'react-redux';
import {addEmployee} from '../../event-creaters';

const mapStateToProps = (state) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addEmployee: emp => dispatch(addEmployee(emp)) 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FormContainer);