import React from 'react';

const FormView = props =>
    <div>
        <table><tbody>
            <tr>
                <td>Name</td>
                <td><input value={props.name} onChange={props.onNameChange} /></td>
            </tr>
            <tr>
                <td>Phone Number</td>
                <td><input value={props.phone} onChange={props.onPhoneChange} /></td>
            </tr>
            <tr>
                <td>Email</td>
                <td><input value={props.email} onChange={props.onEmailChange} /></td>
            </tr>
            <tr>
                <td><button onClick={props.onRegister}>Register</button></td>
            </tr>
        </tbody></table>
    </div>

export default FormView;