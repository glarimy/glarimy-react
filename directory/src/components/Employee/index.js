import EmployeeContainer from './EmployeeContainer';
import {connect} from 'react-redux';
import {viewEmployee, removeEmployee} from '../../event-creaters';

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        viewEmployee: phone=>dispatch(viewEmployee(phone)),
        removeEmployee: phone=>dispatch(removeEmployee(phone)), 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeContainer);