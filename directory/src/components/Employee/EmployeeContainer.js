import React from 'react';
import EmployeeView from './EmployeeView';

class EmployeeContainer extends React.Component {
    constructor(props){
        super(props);
        this.onView = event => this.props.viewEmployee(this.props.data.phone);
        this.onRemove = event => this.props.removeEmployee(this.props.data.phone);
    }

    componentWillReceiveProps() {
        console.log('employee receive props');
    }

    componentDidMount() {
        console.log('employee mounted');
    }

    componentDidUpdate() {
        console.log('employee updated');
    }

    componentWillUnmount() {
        console.log('employee will be unmounted');
    }

    componentDidCatch

    render() {
        return (<EmployeeView 
            index={this.props.index}
            data={this.props.data} 
            onView={this.onView} 
            onRemove={this.onRemove}
        />);
    }
}

export default EmployeeContainer;