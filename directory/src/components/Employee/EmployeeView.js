import React from 'react';

const EmployeeView = props =>
    <tr>
        <td>{props.index}</td>
        <td>{props.data.name}</td>
        <td>{props.data.phone}</td>
        <td><button onClick={props.onView}>View</button></td>
        <td><button onClick={props.onRemove}>Remove</button></td>
    </tr>

export default EmployeeView;