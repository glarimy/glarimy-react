import React from 'react';

const DetailView = props =>
    <table>
        <tbody>
            <tr><td>Name</td><td>{props.employee.name}</td></tr>
            <tr><td>Phone Number</td><td>{props.employee.phone}</td></tr>
            <tr><td>Email</td><td>{props.employee.email}</td></tr>
        </tbody>
    </table>

export default DetailView;