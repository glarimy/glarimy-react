import DetailView from './DetailView';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        employee: state.employee
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DetailView);