import TableContainer from './TableContainer';
import {connect} from 'react-redux';
import {viewEmployee, removeEmployee, addEmployees} from '../../event-creaters';

const mapStateToProps = (state) => {
    return {
        employees: state.employees
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addEmployees: emps=>dispatch(addEmployees(emps)),
        viewEmployee: emp=>dispatch(viewEmployee(emp)),
        removeEmployee: emp=>dispatch(removeEmployee(emp)), 
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(TableContainer);