import React from 'react';
import Employee from '../Employee'

const TableView = props =>
    <table>
        <tbody>
            <tr><td>#</td><td>Name</td><td>Phone</td></tr>
            {
                props.employees.map((e, i) =>
                    <Employee key={i} index={i+1} data={e}/>
                )
            }
        </tbody>
    </table>

export default TableView;