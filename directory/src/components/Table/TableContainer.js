import React from 'react';
import TableView from './TableView';

class TableContainer extends React.Component {
    componentDidMount() {
        console.log('mounted')
        fetch('employees.json').then(response=>
            response.json().then(data=>this.props.addEmployees(data.list) 
        ))
    }

    getSnapshotBeforeUpdate() {
        console.log('snapshot');
        return 0;
    }

    componentDidUpdate() {
        console.log('updated');
    }

    componentWillUnmount() {
        console.log('will be unmounted');
    }

    render() {
        return (
            <TableView employees={this.props.employees}/>
        );
    }
}

export default TableContainer;