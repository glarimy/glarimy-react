import React from 'react';
import Form from '../Form/';
import Table from '../Table/';
import Detail from '../Detail';
import { HashRouter as Router, Route, Link, Switch } from 'react-router-dom';

const DirectoryView = props =>
    <Router>
        <div>
            <h1>Employee Directory</h1>
            <div><Link to='/list'>List Employees</Link> | <Link to='/add'>Add Employee</Link> </div>
            <Switch>
                <Route path='/list' component={Table} />
                <Route path='/add' component={Form} />
                <Route path='/detail' component={Detail} />
            </Switch>
        </div>
    </Router>

export default DirectoryView;